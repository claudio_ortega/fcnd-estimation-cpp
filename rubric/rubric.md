#Rubric for Term1 - Project Estimation

Claudio Ortega

### Rubric list


1) Determine the standard deviation of the measurement noise of both GPS X data and Accelerometer X data.
The calculated standard deviation should correctly capture ~68% of the sensor measurements. 
Your writeup should describe the method used for determining the standard deviation given the 
simulated sensor measurements.

> I've used the formula for mean and variance estimation in any elementary probability text book,
with data copied from the output file from the step into an excel worksheet.
The two results {mean,stdev} where plugged after into the config file.

2) Implement a better rate gyro attitude integration scheme in the UpdateFromIMU() function.
The improved integration scheme should result in an attitude estimator of < 0.1 rad for each of 
the Euler angles for a duration of at least 3 seconds during the simulation. 
The integration scheme should use quaternions to improve performance over the current 
simple integration scheme.

> I've used an intuitive formula for integrating on the quaternion 
for the rotation itself that was shared over the slack channel [2] 
I believe from a hint provided in the very same source 
code [3] that we given with as a starting point for this step.

    Quaternion<float> qest = Quaternion<float>::FromEuler123_RPY(rollEst, pitchEst, ekfState(6));
    qest.IntegrateBodyRate( gyro, dtIMU );

3) Implement all of the elements of the prediction step for the estimator.
The prediction step should include the state update element (PredictState() function), 
a correct calculation of the Rgb prime matrix, and a proper update of the state covariance. 
The acceleration should be accounted for as a command in the calculation of gPrime. 
The covariance update should follow the classic EKF update equation.

> I've followed [1] 3, 7.2,[4],[5].

4) Implement the magnetometer update.
The update should properly include the magnetometer data into the state. Note that the 
solution should make sure to correctly measure the angle error between the current state 
and the magnetometer value (error should be the short way around, not the long way).
It was tricky to realize that I had to make the error in psi, and not the values 
measured and estimated for psi the only value that needs to be normalized. 

> I've followed [1] 7.3.2,[4],[5]. 
I got confused by the fact that the students were not required to transform psi from body frame 
into world frame. 

5) Implement the GPS update.
The estimator should correctly incorporate the GPS information to update the current 
state estimate.

> I followed [1] 7.3.1,[4],[5].

6) Meet the performance criteria of each step.
For each step of the project, the final estimator should be able to successfully meet 
the performance criteria with the controller provided. The estimator's parameters should 
be properly adjusted to satisfy each of the performance criteria elements.

> My code passes the performance criteria defined for all steps.

7) De-tune your controller to successfully fly the final desired box trajectory with your 
estimator and realistic sensors.
The controller developed in the previous project should be de-tuned to successfully 
meet the performance criteria of the final scenario (<1m error for entire box flight).

> My code passes the performance criteria defined for all steps.


### Separate achievements:
1) I managed to make the project run/debug inside CLion under MAC. 
The CMakeLists.txt file provided in the course material for Linux does not work under MAC OS X, 
I needed to come up with my own file, which I make available for the course instructors 
from the zip file of this submission.


### Suggestions:
1) Include the LaTex source for [1] as a file under [3]

2) The explanation given in [1], section 7.1.2 Nonlinear complementary filter,
    is insufficient to get into what others and I ended up using. 
    That section should be fixed. Here a non exhaustive list of reasons:
    
- it uses an undefined term, theta/phi_bar inside the same formulas (46,47)  

- it is difficult, if not impossible, to get into the solution it was 
shared over [2]. My suggestion is not to take shortcuts by just hinting in the comments 
in the source code something as important as this. All that should be in [1].

- it misleads into using a mix alfa, 1-alfa, as for the linear case, but in the 
end it does not use it, or at least nobody I talked to got how.


### References:

[1] Estimation for Quadrotors, Tellex et al, May 2018

[2] Slack channel for the course

[3] https://github.com/udacity/FCND-Estimation-CPP.git 

[4] James Diebel. Representing attitude: Euler angles, unit quaternions, and rotation vectors. 
    Matrix, 58 (15-16):1–35, 2006.

[5] Eigen C++ Library tutorial - https://eigen.tuxfamily.org/dox/group__TutorialMatrixArithmetic.html