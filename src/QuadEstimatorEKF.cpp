#include <iostream>
#include "Common.h"
#include "QuadEstimatorEKF.h"
#include "Utility/SimpleConfig.h"
#include "Utility/StringUtils.h"
#include "Math/Quaternion.h"
#include "Math/Angles.h"

using namespace SLR;

const int QuadEstimatorEKF::QUAD_EKF_NUM_STATES;

QuadEstimatorEKF::QuadEstimatorEKF(string config, string name)
  : BaseQuadEstimator(config),
  Q(QUAD_EKF_NUM_STATES, QUAD_EKF_NUM_STATES),
  R_GPS(6, 6),
  R_Mag(1, 1),
  ekfState(QUAD_EKF_NUM_STATES),
  ekfCov(QUAD_EKF_NUM_STATES, QUAD_EKF_NUM_STATES),
  trueError(QUAD_EKF_NUM_STATES)
{
  _name = name;
  Init();
}

QuadEstimatorEKF::~QuadEstimatorEKF()
{

}

void QuadEstimatorEKF::Init()
{
  ParamsHandle paramSys = SimpleConfig::GetInstance();

  paramSys->GetFloatVector(_config + ".InitState", ekfState);

  VectorXf initStdDevs(QUAD_EKF_NUM_STATES);
  paramSys->GetFloatVector(_config + ".InitStdDevs", initStdDevs);
  ekfCov.setIdentity();
  for (int i = 0; i < QUAD_EKF_NUM_STATES; i++)
  {
    ekfCov(i, i) = initStdDevs(i) * initStdDevs(i);
  }

  // complementary filter params
  attitudeTau = paramSys->Get(_config + ".AttitudeTau", .1f);
  dtIMU = paramSys->Get(_config + ".dtIMU", .002f);

  pitchEst = 0;
  rollEst = 0;
  
  // GPS measurement model covariance
  R_GPS.setZero();
  R_GPS(0, 0) = R_GPS(1, 1) = powf(paramSys->Get(_config + ".GPSPosXYStd", 0), 2);
  R_GPS(2, 2) = powf(paramSys->Get(_config + ".GPSPosZStd", 0), 2);
  R_GPS(3, 3) = R_GPS(4, 4) = powf(paramSys->Get(_config + ".GPSVelXYStd", 0), 2);
  R_GPS(5, 5) = powf(paramSys->Get(_config + ".GPSVelZStd", 0), 2);

  // magnetometer measurement model covariance
  R_Mag.setZero();
  R_Mag(0, 0) = powf(paramSys->Get(_config + ".MagYawStd", 0), 2);

  // load the transition model covariance
  Q.setZero();
  Q(0, 0) = Q(1, 1) = powf(paramSys->Get(_config + ".QPosXYStd", 0), 2);
  Q(2, 2) = powf(paramSys->Get(_config + ".QPosZStd", 0), 2);
  Q(3, 3) = Q(4, 4) = powf(paramSys->Get(_config + ".QVelXYStd", 0), 2);
  Q(5, 5) = powf(paramSys->Get(_config + ".QVelZStd", 0), 2);
  Q(6, 6) = powf(paramSys->Get(_config + ".QYawStd", 0), 2);
  Q *= dtIMU;

  rollErr = pitchErr = maxEuler = 0;
  posErrorMag = velErrorMag = 0;
}


void QuadEstimatorEKF::UpdateFromIMU(V3F accel, V3F gyro)
{
  // Improve a complementary filter-type attitude filter
  // 
  // Currently a small-angle approximation integration method is implemented
  // The integrated (predicted) value is then updated in a complementary filter style with attitude information from accelerometers
  // 
  // Implement a better integration method that uses the current attitude estimate (rollEst, pitchEst and ekfState(6))
  // to integrate the body rates into new Euler angles.
  //
  // HINTS:
  //  - there are several ways to go about this, including:
  //    1) create a rotation matrix based on your current Euler angles, integrate that, convert back to Euler angles
  //    OR 
  //    2) use the Quaternion<float> class, which has a handy FromEuler123_RPY function for creating a quaternion from Euler Roll/PitchYaw
  //       (Quaternion<float> also has a IntegrateBodyRate function, though this uses quaternions, not Euler angles)

  // these are needed for graphics
  accelRoll = atan2f(accel.y, accel.z);
  accelPitch = atan2f(-accel.x, 9.81f);

  // non linear case, following some fellow's advise, but NOT the same as in the paper, formulas 46,47
  // ekfState(6) : yaw
  Quaternion<float> qest = Quaternion<float>::FromEuler123_RPY(rollEst, pitchEst, ekfState(6));
  qest.IntegrateBodyRate( gyro, dtIMU );

  rollEst = qest.Roll();
  pitchEst = qest.Pitch();
  ekfState(6) = AngleNormF(qest.Yaw());

  lastGyro = gyro;
}

void QuadEstimatorEKF::UpdateTrueError(V3F truePos, V3F trueVel, Quaternion<float> trueAtt)
{
  VectorXf trueState(QUAD_EKF_NUM_STATES);
  trueState(0) = truePos.x;
  trueState(1) = truePos.y;
  trueState(2) = truePos.z;
  trueState(3) = trueVel.x;
  trueState(4) = trueVel.y;
  trueState(5) = trueVel.z;
  trueState(6) = trueAtt.Yaw();

  trueError = ekfState - trueState;
  trueError(6) = AngleNormF(trueError(6));
  pitchErr = AngleNormF(pitchEst - trueAtt.Pitch());
  rollErr = AngleNormF(rollEst - trueAtt.Roll());
  maxEuler = MAX(fabs(pitchErr), MAX(fabs(rollErr), fabs(trueError(6))));

  posErrorMag = truePos.dist(V3F(ekfState(0), ekfState(1), ekfState(2)));
  velErrorMag = trueVel.dist(V3F(ekfState(3), ekfState(4), ekfState(5)));
}

// Predict the current state forward by time dt using current accelerations and body rates as input
// INPUTS:
//   curState: starting state
//   dt: time step to predict forward by [s]
//   accel: acceleration of the vehicle, in body frame, *not including gravity* [m/s2]
//   gyro: body rates of the vehicle, in body frame [rad/s]
//
// OUTPUT:
//   return the predicted state as a vector
VectorXf QuadEstimatorEKF::PredictState(
  VectorXf curState,
  float dt,
  V3F accelInBodyFrame,
  V3F omegaInBodyFrame)   // why is this parameter passed in?
{
  assert(curState.size() == QUAD_EKF_NUM_STATES);

  const float x_t_x = curState(0);
  const float x_t_y = curState(1);
  const float x_t_z = curState(2);
  const float x_t_xDot = curState(3);
  const float x_t_yDot = curState(4);
  const float x_t_zDot = curState(5);
  const float x_t_yaw = curState(6);

  const Quaternion<float> attitude = Quaternion<float>::FromEuler123_RPY(
    rollEst,
    pitchEst,
    x_t_yaw);

  const V3F accelInWorldFrame = attitude.Rotate_BtoI( accelInBodyFrame );

  VectorXf predictedState(7);
  predictedState(0) = x_t_x + x_t_xDot * dt;
  predictedState(1) = x_t_y + x_t_yDot * dt;
  predictedState(2) = x_t_z + x_t_zDot * dt;
  predictedState(3) = x_t_xDot + accelInWorldFrame.x * dt;
  predictedState(4) = x_t_yDot + accelInWorldFrame.y * dt;
  predictedState(5) = x_t_zDot - (float)CONST_GRAVITY * dt + accelInWorldFrame.z * dt ;
  predictedState(6) = x_t_yaw; // no need to re-compute x_t_yaw as it was already computed inside UpdateFromIMU()

  return predictedState;
}

// Return the partial derivative of the Rbg rotation matrix with respect to yaw.
// INPUTS:
//   roll, pitch, yaw: Euler angles at which to calculate RbgPrime
// OUTPUT:
//   return the 3x3 matrix representing the partial derivative at the given point
MatrixXf GetRbgPrime(float phi, float theta, float psi)
{
  MatrixXf RbgPrime(3,3);

  RbgPrime.coeffRef(0,0) =            -cos(theta) * sin(psi);
  RbgPrime.coeffRef(1,0) =             cos(theta) * cos(psi);
  RbgPrime.coeffRef(2,0) = 0;

  RbgPrime.coeffRef(0,1) = -sin(phi) * sin(theta) * sin(psi) - cos(phi) * cos(psi);
  RbgPrime.coeffRef(1,1) =  sin(phi) * sin(theta) * cos(psi) - cos(phi) * sin(psi);
  RbgPrime.coeffRef(2,1) = 0;

  RbgPrime.coeffRef(0,2) = -cos(phi) * sin(theta) * sin(psi) + sin(phi) * cos(psi);
  RbgPrime.coeffRef(1,2) =  cos(phi) * sin(theta) * cos(psi) + sin(phi) * sin(psi);
  RbgPrime.coeffRef(2,2) = 0;

  return RbgPrime;
}

MatrixXf GetGPrime( float roll, float pitch, float yaw, float dt, V3F accel, int size )
{
  const MatrixXf rbgPrime = GetRbgPrime(roll, pitch, yaw);
  VectorXf accelInBodyFrame(3);
  accelInBodyFrame << accel.x, accel.y, accel.z;
  const VectorXf accelTransformed = rbgPrime * accelInBodyFrame;

  MatrixXf gPrime(size, size);
  gPrime.setIdentity();
  gPrime(0,3) = dt;
  gPrime(1,4) = dt;
  gPrime(2,5) = dt;
  gPrime(3,6) = accelTransformed(0) * dt;
  gPrime(4,6) = accelTransformed(1) * dt;
  gPrime(5,6) = accelTransformed(2) * dt;

  return gPrime;
}

// INPUTS:
//   dt: time step to predict forward by [s]
//   accel: acceleration of the vehicle, in body frame, *not including gravity* [m/s2]
//   gyro: body rates of the vehicle, in body frame [rad/s]
//   state (member variable): current state (state at the beginning of this prediction)
//
// OUTPUT:
//   update the member variable cov to the predicted covariance
void QuadEstimatorEKF::Predict(float dt, V3F accel, V3F gyro)
{
  // predict the mean of the state forward by dt using the current accelerations as input.
  const VectorXf newState = PredictState(ekfState, dt, accel, gyro);
  // predict the state covariance forward by dt using the current accelerations and body rates as input.
  const MatrixXf gPrime = GetGPrime(rollEst, pitchEst, ekfState(6), dt, accel, QUAD_EKF_NUM_STATES);

  // update state
  ekfState = newState;

  ekfCov = gPrime * ekfCov * gPrime.transpose() + Q;
}

// GPS UPDATE
//  - The GPS measurement covariance is available in member variable R_GPS
void QuadEstimatorEKF::UpdateFromGPS(
    V3F measuredPosition,
    V3F measuredVelocity)
{
  MatrixXf hPrime(6, QUAD_EKF_NUM_STATES);
  hPrime.setIdentity();  // setIdentity() sets 1's on the diagonal 0,0-5,5, leaving the last column (6) all on 0's

  VectorXf z(6);
  z(0) = measuredPosition.x;
  z(1) = measuredPosition.y;
  z(2) = measuredPosition.z;
  z(3) = measuredVelocity.x;
  z(4) = measuredVelocity.y;
  z(5) = measuredVelocity.z;

  VectorXf zFromX(6);
  zFromX(0) = ekfState(0);
  zFromX(1) = ekfState(1);
  zFromX(2) = ekfState(2);
  zFromX(3) = ekfState(3);
  zFromX(4) = ekfState(4);
  zFromX(5) = ekfState(5);

  Update(z, hPrime, R_GPS, zFromX);
}

// MAGNETOMETER UPDATE
void QuadEstimatorEKF::UpdateFromMag(float measuredYaw)
{
  MatrixXf hPrime(1, QUAD_EKF_NUM_STATES);
  hPrime.setZero();
  hPrime(0, QUAD_EKF_NUM_STATES-1) = 1;

  VectorXf z(1);
  z(0) = AngleNormF(measuredYaw);

  VectorXf zFromX(1);
  zFromX(0) = ekfState(6);

  // super tricky thing here,
  // having both z, zFromX folded could get big errors,
  // so we let go the folding of next zFromX by folding the -difference-
  // by adjusting zFromX, to guarantee that the difference is always small
  float diff = z(0) - zFromX(0);

  if ( diff > F_PI )
  {
    // correct measurement
    z(0) -= 2.0*F_PI;
  }

  else if ( diff < -F_PI )
  {
    // correct measurement
    z(0) += 2.0*F_PI;
  }

  Update(z, hPrime, R_Mag, zFromX);
}

// Execute an EKF update step
// z: measurement
// H: Jacobian of observation function evaluated at the current estimated state
// R: observation error model covariance 
// zFromX: measurement prediction based on current state
void QuadEstimatorEKF::Update(VectorXf& z, MatrixXf& H, MatrixXf& R, VectorXf& zFromX)
{
  assert(z.size() == H.rows());
  assert(QUAD_EKF_NUM_STATES == H.cols());
  assert(z.size() == R.rows());
  assert(z.size() == R.cols());
  assert(z.size() == zFromX.size());

  MatrixXf toInvert(z.size(), z.size());
  toInvert = H * ekfCov * H.transpose() + R;
  MatrixXf K = ekfCov * H.transpose() * toInvert.inverse();

  ekfState = ekfState + K*(z - zFromX);

  MatrixXf eye(QUAD_EKF_NUM_STATES, QUAD_EKF_NUM_STATES);
  eye.setIdentity();

  ekfCov = (eye - K*H)*ekfCov;
}

// Calculate the condition number of the EKF ovariance matrix (useful for numerical diagnostics)
// The condition number provides a measure of how similar the magnitudes of the error metric beliefs 
// about the different states are. If the magnitudes are very far apart, numerical issues will start to come up.
float QuadEstimatorEKF::CovConditionNumber() const
{
  MatrixXf m(7, 7);
  for (int i = 0; i < 7; i++)
  {
    for (int j = 0; j < 7; j++)
    {
      m(i, j) = ekfCov(i, j);
    }
  }

  Eigen::JacobiSVD<MatrixXf> svd(m);
  float cond = svd.singularValues()(0)
    / svd.singularValues()(svd.singularValues().size() - 1);
  return cond;
}

// Access functions for graphing variables
bool QuadEstimatorEKF::GetData(const string& name, float& ret) const
{
  if (name.find_first_of(".") == string::npos) return false;
  string leftPart = LeftOf(name, '.');
  string rightPart = RightOf(name, '.');

  if (ToUpper(leftPart) == ToUpper(_name))
  {
#define GETTER_HELPER(A,B) if (SLR::ToUpper(rightPart) == SLR::ToUpper(A)){ ret=(B); return true; }
    GETTER_HELPER("Est.roll", rollEst);
    GETTER_HELPER("Est.pitch", pitchEst);

    GETTER_HELPER("Est.x", ekfState(0));
    GETTER_HELPER("Est.y", ekfState(1));
    GETTER_HELPER("Est.z", ekfState(2));
    GETTER_HELPER("Est.vx", ekfState(3));
    GETTER_HELPER("Est.vy", ekfState(4));
    GETTER_HELPER("Est.vz", ekfState(5));
    GETTER_HELPER("Est.yaw", ekfState(6));

    GETTER_HELPER("Est.S.x", sqrtf(ekfCov(0, 0)));
    GETTER_HELPER("Est.S.y", sqrtf(ekfCov(1, 1)));
    GETTER_HELPER("Est.S.z", sqrtf(ekfCov(2, 2)));
    GETTER_HELPER("Est.S.vx", sqrtf(ekfCov(3, 3)));
    GETTER_HELPER("Est.S.vy", sqrtf(ekfCov(4, 4)));
    GETTER_HELPER("Est.S.vz", sqrtf(ekfCov(5, 5)));
    GETTER_HELPER("Est.S.yaw", sqrtf(ekfCov(6, 6)));

    // diagnostic variables
    GETTER_HELPER("Est.D.AccelPitch", accelPitch);
    GETTER_HELPER("Est.D.AccelRoll", accelRoll);

    GETTER_HELPER("Est.D.ax_g", accelG[0]);
    GETTER_HELPER("Est.D.ay_g", accelG[1]);
    GETTER_HELPER("Est.D.az_g", accelG[2]);

    GETTER_HELPER("Est.E.x", trueError(0));
    GETTER_HELPER("Est.E.y", trueError(1));
    GETTER_HELPER("Est.E.z", trueError(2));
    GETTER_HELPER("Est.E.vx", trueError(3));
    GETTER_HELPER("Est.E.vy", trueError(4));
    GETTER_HELPER("Est.E.vz", trueError(5));
    GETTER_HELPER("Est.E.yaw", trueError(6));
    GETTER_HELPER("Est.E.pitch", pitchErr);
    GETTER_HELPER("Est.E.roll", rollErr);
    GETTER_HELPER("Est.E.MaxEuler", maxEuler);

    GETTER_HELPER("Est.E.pos", posErrorMag);
    GETTER_HELPER("Est.E.vel", velErrorMag);

    GETTER_HELPER("Est.D.covCond", CovConditionNumber());
#undef GETTER_HELPER
  }
  return false;
};

vector<string> QuadEstimatorEKF::GetFields() const
{
  vector<string> ret = BaseQuadEstimator::GetFields();
  ret.push_back(_name + ".Est.roll");
  ret.push_back(_name + ".Est.pitch");

  ret.push_back(_name + ".Est.x");
  ret.push_back(_name + ".Est.y");
  ret.push_back(_name + ".Est.z");
  ret.push_back(_name + ".Est.vx");
  ret.push_back(_name + ".Est.vy");
  ret.push_back(_name + ".Est.vz");
  ret.push_back(_name + ".Est.yaw");

  ret.push_back(_name + ".Est.S.x");
  ret.push_back(_name + ".Est.S.y");
  ret.push_back(_name + ".Est.S.z");
  ret.push_back(_name + ".Est.S.vx");
  ret.push_back(_name + ".Est.S.vy");
  ret.push_back(_name + ".Est.S.vz");
  ret.push_back(_name + ".Est.S.yaw");

  ret.push_back(_name + ".Est.E.x");
  ret.push_back(_name + ".Est.E.y");
  ret.push_back(_name + ".Est.E.z");
  ret.push_back(_name + ".Est.E.vx");
  ret.push_back(_name + ".Est.E.vy");
  ret.push_back(_name + ".Est.E.vz");
  ret.push_back(_name + ".Est.E.yaw");
  ret.push_back(_name + ".Est.E.pitch");
  ret.push_back(_name + ".Est.E.roll");

  ret.push_back(_name + ".Est.E.pos");
  ret.push_back(_name + ".Est.E.vel");

  ret.push_back(_name + ".Est.E.maxEuler");

  ret.push_back(_name + ".Est.D.covCond");

  // diagnostic variables
  ret.push_back(_name + ".Est.D.AccelPitch");
  ret.push_back(_name + ".Est.D.AccelRoll");
  ret.push_back(_name + ".Est.D.ax_g");
  ret.push_back(_name + ".Est.D.ay_g");
  ret.push_back(_name + ".Est.D.az_g");
  return ret;
};
