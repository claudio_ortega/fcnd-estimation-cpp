#include "Common.h"
#include "QuadControl.h"

#include "Utility/SimpleConfig.h"

#include "Utility/StringUtils.h"
#include "Trajectory.h"
#include "BaseController.h"
#include "Math/Angles.h"
#include "Math/Mat3x3F.h"
#include "Math/Constants.h"

#ifdef __PX4_NUTTX
#include <systemlib/param/param.h>
#endif

void QuadControl::Init()
{
  BaseController::Init();

  // variables needed for integral control
  integratedError = NULL_VECTOR_F;
    
#ifndef __PX4_NUTTX
  // Load params from simulator parameter system
  ParamsHandle config = SimpleConfig::GetInstance();
   
  // Load parameters (default to 0)
  kpPosXY = config->Get(_config+".kpPosXY", 0);
  kpPosZ = config->Get(_config + ".kpPosZ", 0);
  kiPosZ = config->Get(_config + ".kiPosZ", 0);

  kpVelXY = config->Get(_config + ".kpVelXY", 0);
  kpVelZ = config->Get(_config + ".kpVelZ", 0);

  kpBank = config->Get(_config + ".kpBank", 0);
  kpYaw = config->Get(_config + ".kpYaw", 0);

  kpPQR = config->Get(_config + ".kpPQR", V3F());

  maxDescentRate = config->Get(_config + ".maxDescentRate", 100);
  maxAscentRate = config->Get(_config + ".maxAscentRate", 100);
  maxSpeedXY = config->Get(_config + ".maxSpeedXY", 100);
  maxAccelXY = config->Get(_config + ".maxHorizAccel", 100);

  maxTiltAngle = config->Get(_config + ".maxTiltAngle", 100);

  minMotorThrust = config->Get(_config + ".minMotorThrust", 0);
  maxMotorThrust = config->Get(_config + ".maxMotorThrust", 100);

  maxYawRate = config->Get(_config + ".maxYawRate", 10.0);
#else
  // load params from PX4 parameter system
  //TODO
  param_get(param_find("MC_PITCH_P"), &Kp_bank);
  param_get(param_find("MC_YAW_P"), &Kp_yaw);
#endif
}

VehicleCommand QuadControl::GenerateMotorCommands(
    float collThrustCmd,
    V3F momentCmd)
{
  // Convert a desired 3-axis moment and collective thrust command to 
  //   individual motor thrust commands
  // INPUTS:
  //   collThrustCmd: desired collective thrust [N]
  //   momentCmd: desired rotation moment about each axis [N m]
  // OUTPUT:
  //   set class member variable cmd (class variable for graphing) where
  //   cmd.desiredThrustsN[0..3]: motor commands, in [N]

  // L is distance motor-CM, l is distance motor-both X/Y axes
  float l = L * sqrt(0.5);
  float a1 = momentCmd.x / l;
  float a2 = momentCmd.y / l;
  float a3 = momentCmd.z / kappa;
  float a4 = collThrustCmd;

  cmd.desiredThrustsN[0] = (   a1 + a2 - a3 + a4 ) * 0.25f; // front left
  cmd.desiredThrustsN[1] = ( - a1 + a2 + a3 + a4 ) * 0.25f; // front right
  cmd.desiredThrustsN[2] = (   a1 - a2 + a3 + a4 ) * 0.25f; // rear left
  cmd.desiredThrustsN[3] = ( - a1 - a2 - a3 + a4 ) * 0.25f; // rear right

  return cmd;
}

V3F QuadControl::BodyRateControl(
    V3F pqrCmd,
    V3F pqr)
{
  // Calculate a desired 3-axis moment given a desired and current body rate
  // INPUTS: 
  //   pqrCmd: desired body rates [rad/s]
  //   pqr: current or estimated body rates [rad/s]
  // OUTPUT:
  //   return a V3F containing the desired moments for each of the 3 axes in [N m]
  return kpPQR * (pqrCmd - pqr) * V3F(Ixx,Iyy,Izz);
}

// returns a desired roll and pitch rate 
V3F QuadControl::RollPitchControl(
    V3F accelCmd,
    Quaternion<float> attitude,
    float collThrustCmd)
{
  // Calculate a desired pitch and roll angle rates based on a desired global
  //   lateral acceleration, the current attitude of the quad, and desired
  //   collective thrust command
  // INPUTS: 
  //   accelCmd: desired acceleration in global XY coordinates [m/s2]
  //   attitude: current or estimated attitude of the vehicle
  //   collThrustCmd: desired collective thrust of the quad [N]
  // OUTPUT:
  //   return a V3F containing the desired pitch and roll rates. The Z
  //     element of the V3F should be left at its default value (0)
  // PENDING: limit the tilt angle by constraining b_x_c and b_y_c.

  float c = collThrustCmd / mass;

  float b_x_commanded;
  float b_y_commanded;

  if ( c > 0 )
  {
      b_x_commanded = -CONSTRAIN(accelCmd.x / c, -maxTiltAngle, maxTiltAngle);
      b_y_commanded = -CONSTRAIN(accelCmd.y / c, -maxTiltAngle, maxTiltAngle);
  }
  else
  {
      b_x_commanded = 0;
      b_y_commanded = 0;
  }

  Mat3x3F rot_mat = attitude.RotationMatrix_IwrtB();

  float b_x_actual = rot_mat(0, 2);
  float b_x_commanded_dot = kpBank * (b_x_commanded - b_x_actual);

  float b_y_actual = rot_mat(1, 2);
  float b_y_commanded_dot = kpBank * (b_y_commanded - b_y_actual);

  V3F pqrCmd;
  pqrCmd.x = ( rot_mat(1,0) * b_x_commanded_dot - rot_mat(0,0) * b_y_commanded_dot ) / rot_mat(2, 2);
  pqrCmd.y = ( rot_mat(1,1) * b_x_commanded_dot - rot_mat(0,1) * b_y_commanded_dot ) / rot_mat(2, 2);
  pqrCmd.z = 0.0;

  return pqrCmd;
}

float QuadControl::AltitudeControl(
    float posZCmd,
    float velZCmd,
    float posZ,
    float velZ,
    Quaternion<float> attitude,
    float accelZCmd,
    float dt)
{
  // Calculate desired quad thrust based on altitude setpoint, actual altitude,
  //   vertical velocity setpoint, actual vertical velocity, and a vertical
  //   acceleration feed-forward command
  // INPUTS:
  //   posZCmd, velZCmd: desired vertical position and velocity in NED [m]
  //   posZ, velZ: current vertical position and velocity in NED [m]
  //   accelZCmd: feed-forward vertical acceleration in NED [m/s2]
  //   dt: the time step of the measurements [seconds]
  // OUTPUT:
  //   return a collective thrust command in [N]

  velZCmd = CONSTRAIN(velZCmd, -maxDescentRate, maxAscentRate);

  float errPosZ = posZCmd - posZ;
  float errVelZ = velZCmd - velZ;
  integratedError.z = integratedError.z + errPosZ * dt;

  float u1 = kiPosZ * integratedError.z + kpPosZ * errPosZ + kpVelZ * errVelZ + accelZCmd;
  Mat3x3F rot_mat = attitude.RotationMatrix_IwrtB();

  float b_z_actual = rot_mat(2,2);
  float c = ((float)CONST_GRAVITY - u1) / b_z_actual;

  return mass * c;
}

V3F constrainModulus( V3F v, float max )
{
    if ( v.mag() > max )
    {
        return v * ( max / v.mag() );
    }
    else
    {
        return v;
    }
}

// returns a desired acceleration in global frame
V3F QuadControl::LateralPositionControl(
    V3F posCmd,
    V3F velCmd,
    V3F pos,
    V3F vel,
    V3F accelCmdFF,
    float dt)
{
  // Calculate a commanded horizontal acceleration based on
  //  desired lateral position/velocity/acceleration and current pose
  // INPUTS:
  //   posCmd: desired position, in NED [m]
  //   velCmd: desired velocity, in NED [m/s]
  //   pos: current position, NED [m]
  //   vel: current velocity, NED [m/s]
  //   accelCmdFF: feed-forward acceleration, NED [m/s2]
  // OUTPUT:
  //   return a V3F with desired horizontal accelerations.
  //     the Z component should be 0

  // for this controller everything should be zero in the z dimension
  posCmd.z = 0;
  velCmd.z = 0;
  pos.z = 0;
  vel.z = 0;
  accelCmdFF.z = 0;

  return constrainModulus(
    kpPosXY * ( posCmd - pos ) + kpVelXY * ( velCmd - vel ) + accelCmdFF,
    maxAccelXY);
}

// returns desired yaw rate
float QuadControl::YawControl(
    float yawCmd,
    float yaw)
{
  // Calculate a desired yaw rate to control yaw to yawCmd
  // INPUTS:
  //   yawCmd: commanded yaw [rad]
  //   yaw: current yaw [rad]
  // OUTPUT:
  //   return a desired yaw rate [rad/s]

  float diffYaw = yawCmd - yaw;

  float yawCmdDes = kpYaw * AngleNormF( diffYaw );

  yawCmdDes = CONSTRAIN(yawCmdDes, -maxYawRate, maxYawRate);

  return yawCmdDes;
}

VehicleCommand QuadControl::RunControl(
    float dt,
    float simTime)
{
  curTrajPoint = GetNextTrajectoryPoint(simTime);

  float collThrustCmd = AltitudeControl(
    curTrajPoint.position.z,
    curTrajPoint.velocity.z,
    estPos.z,
    estVel.z,
    estAtt,
    curTrajPoint.accel.z,
    dt);

  // reserve some thrust margin for angle control
  float thrustMargin = 0.1f * (maxMotorThrust - minMotorThrust);

  collThrustCmd = CONSTRAIN(
    collThrustCmd,
    (minMotorThrust + thrustMargin) * 4.0f,
    (maxMotorThrust - thrustMargin) * 4.0f);

  V3F desXYAcc = LateralPositionControl(
    curTrajPoint.position,
    curTrajPoint.velocity,
    estPos,
    estVel,
    curTrajPoint.accel,
    dt);

  V3F desOmega = RollPitchControl(
    desXYAcc,
    estAtt,
    collThrustCmd);

  desOmega.z = YawControl(
    curTrajPoint.attitude.Yaw(),
    estAtt.Yaw());

  V3F desMoment = BodyRateControl(
    desOmega,
    estOmega);

  VehicleCommand localCmd = GenerateMotorCommands(
    collThrustCmd,
    desMoment);

  return localCmd;
}
